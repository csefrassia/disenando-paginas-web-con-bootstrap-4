$('.carousel').carousel({
    interval: 5000
})
//Modals
$('#exampleModal').on('show.bs.modal', function (e) {
    console.log('modal cuando comienza a abrirse')
    $('.login').addClass('text-danger')
})
$('#exampleModal').on('shown.bs.modal', function (e) {
    console.log('modal cuando se terminó de abrir')
})
$('#exampleModal').on('hide.bs.modal', function (e) {
    console.log('modal cuando comienza a ocultarse ')
    $('.login').removeClass('text-danger')
})
$('#exampleModal').on('hidden.bs.modal', function (e) {
    console.log('modal cuando se terminó de ocultar')
})